import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'CallAFriend';
  number : string = "";
  showModal : boolean = false;
  array = ["7","8","9"];
  errorMessage : string;
  typeNumber(e){
    if(this.showModal)
    {
      this.showModal = false;
    }
    if(this.number == "" && this.array.indexOf(e) == -1 )
    {
      this.showModal = true; 
      this.errorMessage = "Number should start with 7,8 or 9";
    }
    else if(this.number.length > 9)
    {
      this.showModal = true; 
      this.errorMessage = "Number should be of 10 digits";
    }
    else if(e=="delete"){
      this.number = this.number.slice(0, -1)
    }
    else{  
    this.number = this.number + e;
    }
  }

  call(){
    if(this.showModal == false && this.number.length == 10)
    alert('Call Successfull');
    else{
      this.showModal = true;
      this.errorMessage = "Enter correct number";
    }
  }
}
